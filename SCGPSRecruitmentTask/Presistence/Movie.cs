﻿using System.ComponentModel.DataAnnotations;

namespace SCGPS.RecruitmentTask.Persistence
{
    public class Movie
    {
        [Key]
        public Int32 Id { get; set; }
        public String? Title { get; set; }
        public String? Year { get; set; }
        public String? Genre { get; set; }
        public String? Actors { get; set; }
        public String? Poster { get; set; }
        public String? imdbRating { get; set; }
        public virtual ICollection<Review>? Reviews { get; set; } = null!;
    }
}