﻿using System.ComponentModel.DataAnnotations;

namespace SCGPS.RecruitmentTask.Persistence
{
    public class Review
    {
        [Key]
        public Int32 Id { get; set; }
        [DataType(DataType.MultilineText)]
        public String Description { get; set; } = "";
        public Int32 MovieId { get; set; }
    }
}