﻿using Microsoft.EntityFrameworkCore;

namespace SCGPS.RecruitmentTask.Persistence
{
    public class RecruitmentTaskDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; } = null!;
        public DbSet<Review> Reviews { get; set; } = null!;

        public RecruitmentTaskDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
