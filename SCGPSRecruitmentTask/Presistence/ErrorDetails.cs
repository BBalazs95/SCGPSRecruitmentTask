﻿namespace SCGPS.RecruitmentTask.Persistence
{
    public class ErrorDetails
    {
        public Int32 StatusCode { get; set; }
        public String Message { get; set; }
    }
}
