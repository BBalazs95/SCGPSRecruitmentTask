﻿using API.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using SCGPS.RecruitmentTask.Persistence;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly ILogger<MoviesController> _logger;
        private readonly IOMDBService _omdbService;
        private readonly IMovieService _movieService;

        public MoviesController(ILogger<MoviesController> logger, IOMDBService omdbService, IMovieService movieService)
        {
            _omdbService = omdbService;
            _logger = logger;
            _movieService = movieService;
        }

        [HttpGet]
        public IEnumerable<Movie> Get()
        {
            _logger.LogDebug("Movies Controller Get");
            StringValues order;
            if (Request.Headers.TryGetValue("order", out order))
                return _movieService.getMovieList(order);
            return _movieService.getMovieList("asc");
        }

        [HttpGet("{id}")]
        public Movie Get(Int32 id)
        {
            _logger.LogDebug("Movies Controller Get by id");
            return _movieService.getMovieListById(id);
        }

        [HttpPost]
        public Movie Post([FromBody] string value)
        {
            _logger.LogDebug("Movies Controller Post body: " + value);

            Movie movie = _omdbService.GetMovieByTitle(value);
            try
            {
                movie = _movieService.saveMovie(movie);
            }
            catch (Exception e)
            {
                throw new Exception("Can't save Movie! err message: " + e.Message);
            }
            return movie;
        }
    }
}
