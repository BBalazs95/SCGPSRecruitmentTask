﻿using SCGPS.RecruitmentTask.Persistence;
using System.Text.Json;

namespace API.Controllers
{
    public class ExceptionHandlerMiddleware
    {
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                _logger.LogError("Unhandeled error: " + e.Message);
                context.Response.StatusCode = 500;
                context.Response.ContentType = "application/json";
                ErrorDetails error = new ErrorDetails
                {
                    StatusCode = 500,
                    Message = e.Message
                };
                await context.Response.WriteAsync(JsonSerializer.Serialize<ErrorDetails>(error));
            }
        }
    }
}
