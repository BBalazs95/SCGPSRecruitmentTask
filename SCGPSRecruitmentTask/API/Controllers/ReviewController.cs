﻿using API.Service;
using Microsoft.AspNetCore.Mvc;
using SCGPS.RecruitmentTask.Persistence;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly ILogger<ReviewController> _logger;
        private readonly IReviewService _reviewService;

        public ReviewController(ILogger<ReviewController> logger, IReviewService reviewService)
        {
            _logger = logger;
            _reviewService = reviewService;
        }

        [HttpGet]
        public IEnumerable<Review> Get()
        {
            _logger.LogDebug("Review Controller Get");
            return _reviewService.getAllReviews();
        }

        [HttpGet("{id}")]
        public IEnumerable<Review> Get(Int32 id)
        {
            _logger.LogDebug("Review Controller Get");
            return _reviewService.getReviewByMovieId(id);
        }

        [HttpPost]
        public Review Post([FromBody] Review value)
        {
            _logger.LogDebug("Review Controller Post body: " + value);
            Review review;
            try
            {
                review = _reviewService.saveReview(value);
            }
            catch (Exception e)
            {
                throw new Exception("Can't save Review! err message: " + e.Message);
            }
            return review;
        }
    }
}
