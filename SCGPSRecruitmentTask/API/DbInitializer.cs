﻿using Microsoft.EntityFrameworkCore;
using SCGPS.RecruitmentTask.Persistence;

namespace SCGPS.RecruitmentTask.API
{
    public static class DbInitializer
    {
        public static void Initialize(RecruitmentTaskDbContext context)
        {
            context.Database.Migrate();
            //context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}
