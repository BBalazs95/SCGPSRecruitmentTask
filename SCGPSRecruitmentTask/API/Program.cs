using API.Controllers;
using API.Service;
using Microsoft.EntityFrameworkCore;
using SCGPS.RecruitmentTask.Persistence;
using System.Data;

var builder = WebApplication.CreateBuilder();

builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
    logging.AddDebug();
});

// Add services to the container.
builder.Services.AddDbContext<RecruitmentTaskDbContext>(options =>
{
    IConfigurationRoot configuration = builder.Configuration;
    DbType dbType = configuration.GetValue<DbType>("DbType");

    options.UseSqlite(configuration.GetConnectionString("SqliteConnection"));

    // Use lazy loading (don't forget the virtual keyword on the navigational properties also)
    options.UseLazyLoadingProxies();
});

builder.Services.AddTransient<IOMDBService, OMDBService>();
builder.Services.AddTransient<IMovieService, MovieService>();
builder.Services.AddTransient<IReviewService, ReviewService>();
builder.Services.AddHttpClient();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionHandlerMiddleware>();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
