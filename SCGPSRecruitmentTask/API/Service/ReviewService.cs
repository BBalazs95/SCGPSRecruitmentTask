﻿using SCGPS.RecruitmentTask.Persistence;

namespace API.Service
{
    public class ReviewService : IReviewService
    {
        private readonly RecruitmentTaskDbContext _context;

        public ReviewService(RecruitmentTaskDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Review> getAllReviews()
        {
            return _context.Reviews;
        }

        public IEnumerable<Review> getReviewByMovieId(int id)
        {
            Movie? movie = _context.Movies.Find(id);
            if (movie == null)
                throw new Exception("No Movie with this id: " + id);
            return movie.Reviews ?? new List<Review>();
        }

        public Review saveReview(Review review)
        {
            Review res;
            res = _context.Add(review).Entity;
            _context.SaveChanges();
            return res;
        }
    }
}
