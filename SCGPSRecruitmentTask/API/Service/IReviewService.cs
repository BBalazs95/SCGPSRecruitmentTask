﻿using SCGPS.RecruitmentTask.Persistence;

namespace API.Service
{
    public interface IReviewService
    {
        Review saveReview(Review review);
        IEnumerable<Review> getReviewByMovieId(Int32 id);
        IEnumerable<Review> getAllReviews();
    }
}
