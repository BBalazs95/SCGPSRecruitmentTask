﻿using SCGPS.RecruitmentTask.Persistence;

namespace API.Service
{
    public interface IMovieService
    {
        Movie saveMovie(Movie movie);
        IEnumerable<Movie> getMovieList(String order);
        public Movie getMovieListById(Int32 id);
    }
}
