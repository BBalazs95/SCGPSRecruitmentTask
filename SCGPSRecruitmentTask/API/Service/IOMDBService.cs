﻿using SCGPS.RecruitmentTask.Persistence;

namespace API.Service
{
    public interface IOMDBService
    {
        Movie GetMovieByTitle(string title);
    }
}
