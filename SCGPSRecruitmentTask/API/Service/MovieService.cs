﻿using SCGPS.RecruitmentTask.Persistence;

namespace API.Service
{
    public class MovieService : IMovieService
    {
        private readonly RecruitmentTaskDbContext _context;

        public MovieService(RecruitmentTaskDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Movie> getMovieList(String order)
        {
            if (order == "asc")
                return _context.Movies.OrderBy(m => m.Title);
            return _context.Movies.OrderByDescending(m => m.Title);
        }

        public Movie getMovieListById(Int32 id)
        {
            Movie? movie = _context.Movies.Find(id);
            if (movie == null)
                throw new Exception("No Movie with this id: " + id);
            return movie;
        }

        public Movie saveMovie(Movie movie)
        {
            if (_context.Movies.FirstOrDefault(m => movie.Title.Equals(m.Title)) != null)
                throw new Exception("Can't save twice a Movie");
            Movie res = _context.Add(movie).Entity;
            _context.SaveChanges();
            return res;
        }
    }
}
