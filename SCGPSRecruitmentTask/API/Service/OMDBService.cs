﻿using SCGPS.RecruitmentTask.Persistence;
using System.Text.Json;

namespace API.Service
{
    public class OMDBService : IOMDBService
    {
        private static IHttpClientFactory? _httpClientFactory;
        private readonly ILogger<OMDBService> _logger;

        public OMDBService(ILogger<OMDBService> logger, IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        public Movie GetMovieByTitle(string title)
        {
            string url = $"http://www.omdbapi.com/?apikey=cc4532bf&t={title}";
            HttpClient httpClient = _httpClientFactory.CreateClient();
            HttpResponseMessage response = httpClient.GetAsync(url).Result;
            response.EnsureSuccessStatusCode();
            string responseBody = response.Content.ReadAsStringAsync().Result;
            _logger.LogDebug("GetMovieByTitle responsebody: " + responseBody);
            return JsonSerializer.Deserialize<Movie>(responseBody) ?? throw new Exception("response body can not be converted to Movie object!");
        }
    }
}
